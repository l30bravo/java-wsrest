package cl.paygroup.test;

import static org.junit.Assert.assertEquals;
import org.junit.Test;

import cl.paygroup.function.ID;
import junit.framework.Assert;

public class TestID {

	@Test
	public void testCheckID(){
		Assert.assertEquals(ID.getID(), "as1234567");
	}
}
