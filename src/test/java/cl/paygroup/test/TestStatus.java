package cl.paygroup.test;

import static org.junit.Assert.assertEquals;
import org.junit.Test;

import cl.paygroup.function.Status;
import junit.framework.Assert;

public class TestStatus {
	
	@Test
	public void testCheckStatus(){
		Assert.assertEquals(Status.getStatus(), "running");
	}
}
