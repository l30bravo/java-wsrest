package cl.paygroup.rest;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import cl.paygroup.function.ID;


public class Index extends HttpServlet{

	static Logger LOGGER = Logger.getLogger(Index.class);

	//POST
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException{
		LOGGER.info("WS POST");
		//response.getWriter().write("WS - POST");
	}

	//GET
	public void doGet(HttpServletRequest req, HttpServletResponse response) throws ServletException, IOException {
		LOGGER.info("WS GET");
		response.setContentType("text/html");
		String json="{  \"nombre\": \"Juanito\",   \"ciudad\": \"Santiago\",   \"productos\": [ 	\"CD001\",  \"CD002\",	\"CD003\"   ] }";
		PrintWriter out = response.getWriter();
		//ID id = new ID();
		/*
		out.print("<center>");
		out.print("<b>WS  REST [ID:"+ID.getID()+"]</b>");
		out.print("<p>Test JENKINS #13 TEST");
		out.print("</center>");
		*/
		out.print(json);
		out.flush();
		out.close();
	}
}
