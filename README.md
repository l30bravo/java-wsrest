# WS REST Java
Proyecto base para crear WS REST con Java

# Agregar vistas  / paginas

* Agregar en cl.paygroup.rest Pagina.java
* Agregar en src/main/webapp/WEB-INF/web.xml 

```xml
	<servlet>
		<servlet-name>Index</servlet-name>
		<servlet-class>
			cl.paygroup.rest.Index
		</servlet-class>
		<load-on-startup>1</load-on-startup>
	</servlet>
```

```xml
	<servlet-mapping>
		<servlet-name>Index</servlet-name>
		<url-pattern>/</url-pattern>
	</servlet-mapping>
```
